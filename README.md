# R.E.B.A.R - <ins>R</ins>ebar <ins>E</ins>nhanced <ins>B</ins>ar that <ins>A</ins>lways Wo<ins>r</ins>ks

Even though the name might be a stretch, the project is not. I have been using Waybar and Polybar for a while
now, and I would like a combination of both. I couldn't find anything that would run under Wayland and X11,
so I decided to build this. Right now, it is early in development. There will be many breaking api/config
changes. The goal is that the bar will look identical on X11 and Wayland, making it very easy to switch from
i3 to sway.

As for individual modules, I don't plan to do anything particularly interesting. I want everything from
Polybar and Waybar, plus a wifi selector module. Of course, I will think of something more I should add
right after I commit this readme. Each of these modules are relatively simple to implement, and only
require a bit of custom socket or /dev functionality in each of them. Waybar also has hover events, and
builtin animations (things like fade to color over x seconds), which I would like to implement.

Another option for custom modules is dynamic linking. Sometime in the future, I plan to add a config
directory that can have a bunch of modules stored in `.so` files. This will enable anyone to add onto this
bar without recompiling it.

### How to use it

There is a config file at `~/.config/rebar/config.yml`. You can see an example of this in `sample_config.yml`.
I chose a yaml file because there is a lot of structure required for this type of config, and I think yaml
looks much nicer than something like json. You can see how this is handled in `src/config.rs`.

Starting Polybar is a pain. You need to have a whole shellscript that finds all your monitors. And if you want
the same bar on multiple screens, you need to set environment variables and a bunch of other nonsense. I plan
to have the monitor configuration built into the config file, so you should just be able to start rebar
without any arguments, and it will spawn all the bars you want.

Logs will go somewhere in `~/.local/share/rebar`. I don't know how this will work, but I know it won't be going
to stdout. If you run `rebar &`, there will only be a few lines of output (probably saying where the log is)
before it goes silent.

### How it works

Everything is based around a struct called `Bar`. This bar manages all the modules, and passes events around.
It also has a `Box<dyn Window>`, which is how everything is rendered. There are two backends: one for X11,
and one for Wayland. This backend manages anything that is specific to either of these systems: handling
any events coming from X11/Wayland, creating the window for the bar, and rendering the modules onto the window.

Each of the modules will be rendered using a `Buffer` trait. This trait is able to render text, boxes, and
other simple shapes. It will wrap a pixmap on X11, and a memory-mapped file on Wayland. This means that any
time a module needs to be redrawn, it can do so without redrawing any other module. All of these pixmaps mean
I won't implement double buffering for the whole bar, as that would just slow things down. The double buffering
will just be implemented by the fact that each module will get it's entire contents copied onscreen at one time.
Since modules don't need to update at the same time as other modules, I think a little bit of desync will
look fine.

### Contributing

Send a PR. Be nice. If it adds a module, I will probably merge it. If it just improves performance, I will most
likely merge it. If it changes the way a current module works, or it changes the backend api, I will be a lot
less likely to accept it. I don't plan on having versioned config files, so any changes must be backwards
compatible with the current config file (assuming I get to a stable spec for the config file. Right now, that
file is due to change at any time).

### Bugs

There will have many bugs. Even with rust, I depend on a lot of C libraries, which means there will be some
segfaults at some point. If you find any error that is not handled nicely (panics are considered not being handled
nicely), then it is probably a bug. Before creating an issue, do a quick search through the closed issues to
see if someone else had the same problem. If no one has, then please report it. Just make sure to share your
config file, and what window manager you are using. If you just post an error message, there is very little
I can do to re-create the issue on my end.

### License

MIT. I would like it if people would keep forks open source, but it's fine if they don't.

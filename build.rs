use std::env;
use std::process::Command;

fn main() {
  println!("cargo:rerun-if-changed=lib/default_modules");
  Command::new("cargo")
    .arg("build")
    .current_dir(env::current_dir().unwrap().join("lib/default_modules"))
    .output()
    .unwrap();
}

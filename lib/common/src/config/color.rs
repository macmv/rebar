use crate::{config, config::ConfigError, Color};

macro_rules! try_parse {
  ($v: expr, $path: expr, $reason: expr, $expected: expr, $extra: expr) => {
    match $v {
      Ok(v) => v,
      Err(_) => {
        return Err(config::parse_err($path, $reason, $expected, $extra));
      }
    }
  };
}

pub fn parse(path: &[&str], text: &str) -> Result<Color, ConfigError> {
  let mut col_str = text.trim();
  let col = col_str.to_string();
  let extra = "expected a color formatted like #rgb, #argb, #rrggbb, or #aarrggbb".into();
  if text.chars().nth(0) != Some('#') {
    return Err(config::parse_err(path, "is invalid color", col, extra));
  }
  col_str = &col_str[1..];
  let msg = "invalid color";
  match col_str.len() {
    3 => {
      // The format #rgb
      let mut red = try_parse!(u8::from_str_radix(&col_str[0..1], 16), path, msg, col, extra);
      let mut green = try_parse!(u8::from_str_radix(&col_str[1..2], 16), path, msg, col, extra);
      let mut blue = try_parse!(u8::from_str_radix(&col_str[2..3], 16), path, msg, col, extra);
      red = red * 16 + red;
      green = green * 16 + green;
      blue = blue * 16 + blue;
      Ok(Color::from_argb(0xff, red, green, blue))
    }
    4 => {
      // The format #argb
      let mut alpha = try_parse!(u8::from_str_radix(&col_str[0..1], 16), path, msg, col, extra);
      let mut red = try_parse!(u8::from_str_radix(&col_str[1..2], 16), path, msg, col, extra);
      let mut green = try_parse!(u8::from_str_radix(&col_str[2..3], 16), path, msg, col, extra);
      let mut blue = try_parse!(u8::from_str_radix(&col_str[3..4], 16), path, msg, col, extra);
      alpha = alpha * 16 + alpha;
      red = red * 16 + red;
      green = green * 16 + green;
      blue = blue * 16 + blue;
      Ok(Color::from_argb(alpha, red, green, blue))
    }
    6 => {
      // The format #rrggbb
      let red = try_parse!(u8::from_str_radix(&col_str[0..2], 16), path, msg, col, extra);
      let green = try_parse!(u8::from_str_radix(&col_str[2..4], 16), path, msg, col, extra);
      let blue = try_parse!(u8::from_str_radix(&col_str[4..6], 16), path, msg, col, extra);
      Ok(Color::from_argb(0xff, red, green, blue))
    }
    8 => {
      // The format #aarrggbb
      let alpha = try_parse!(u8::from_str_radix(&col_str[0..2], 16), path, msg, col, extra);
      let red = try_parse!(u8::from_str_radix(&col_str[2..4], 16), path, msg, col, extra);
      let green = try_parse!(u8::from_str_radix(&col_str[4..6], 16), path, msg, col, extra);
      let blue = try_parse!(u8::from_str_radix(&col_str[6..8], 16), path, msg, col, extra);
      Ok(Color::from_argb(alpha, red, green, blue))
    }
    _ => {
      return Err(config::parse_err(path, msg, col, extra));
    }
  }
}

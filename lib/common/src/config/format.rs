use crate::config;

use std::{collections::HashMap, fmt::Write};

pub struct Formatted {
  pub text: String,
  pub sections: Vec<Section>,
}
pub struct Section {
  pub text: String,
  pub name: String,
  pub start: usize,
  pub end: usize,
}

pub fn parse(
  path: &[&str],
  text: &str,
  args: HashMap<String, String>,
) -> Result<Formatted, config::ConfigError> {
  let chars = text.chars();
  let mut escaping = false;
  // The most recent {. Used to fill in variables from args.
  let mut start = None;
  // Resulting string.
  let mut res = "".to_string();
  // let mut sections = vec![];
  for (i, c) in chars.enumerate() {
    if escaping {
      escaping = false;
      res.push(c);
      continue;
    }
    if c == '\\' {
      escaping = true
    } else if c == '{' {
      start = Some(i + 1);
    } else if c == '}' {
      match start {
        Some(s) => {
          // The value between start (a {) and current (a })
          let name = &text[s..i];
          if !args.contains_key(name) {
            if args.len() == 0 {
              return Err(config::parse_err(
                path,
                "has unknown key",
                name.to_string(),
                "there are no valid keys.".into(),
              ));
            } else {
              return Err(config::parse_err(
                path,
                "has unknown key",
                name.to_string(),
                format!(
                  "valid keys: {}",
                  args.iter().map(|(k, _)| k.to_string() + ", ").collect::<String>()
                ),
              ));
            }
          }
          write!(res, "{}", args[name]).unwrap();
        }
        None => {}
      }
      start = None;
    } else {
      if start.is_none() {
        res.push(c);
      }
    }
  }
  Ok(Formatted { text: res, sections: vec![] })
}

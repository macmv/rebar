use crate::config::format;

use std::collections::HashMap;

#[test]
fn parses_simple() {
  let path = &["path", "name"];

  let mut args = HashMap::new();
  args.insert("first-word".into(), "Hello".into());
  args.insert("second".into(), "world".into());
  let res = format::parse(path, "{first-word} {second}!", args).unwrap();

  assert_eq!(res.text, "Hello world!");
}

#[test]
fn parses_escapes() {
  let path = &["path", "name"];

  let mut args = HashMap::new();
  args.insert("first-word".into(), "Hello".into());
  args.insert("second".into(), "world".into());
  let res = format::parse(path, "\\{{first-word} {second}!", args).unwrap();

  assert_eq!(res.text, "{Hello world!");
}

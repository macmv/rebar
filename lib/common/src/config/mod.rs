extern crate dirs;
extern crate yaml_rust;

pub mod color;
pub mod format;
#[cfg(test)]
pub mod format_test;
pub mod prefixed;

use crate::Color;
use log::error;
use std::{collections::HashMap, error::Error, fmt, fs, process};
use yaml_rust::{yaml::Hash, Yaml, YamlLoader};

pub use prefixed::PrefixedConfig;

pub struct Config {
  yml: Hash,
}

fn path_str(path: &[&str], index: usize) -> String {
  let mut path_str = "".to_string();
  for (i, s) in path[0..index + 1].iter().enumerate() {
    path_str += s;
    if i != index {
      path_str += ".";
    }
  }
  path_str
}

#[derive(Debug)]
pub enum ConfigError {
  Key { path: String, expected: String },
  Exist { path: String },
  Path { path: String },
  Parse { reason: String, value: String, path: String, extra: String },
  Type { path: String, expected: String },
}

impl Error for ConfigError {}
impl fmt::Display for ConfigError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match self {
      ConfigError::Key { path, expected } => {
        write!(f, "key error: key \"{}\" is not a {}", path, expected)
      }
      ConfigError::Exist { path } => {
        write!(f, "exist error: key \"{}\" does not exist", path)
      }
      ConfigError::Path { path } => {
        if path.len() == 0 {
          write!(f, "path error: empty path")
        } else {
          write!(f, "path error: invalid path \"{}\"", path)
        }
      }
      ConfigError::Parse { reason, value, path, extra } => {
        write!(f, "parse error: the value at \"{}\" {}: {}. {}", path, reason, value, extra)
      }
      ConfigError::Type { path, expected } => {
        write!(f, "type error: the value at \"{}\" is not a {}", path, expected)
      }
    }
  }
}

fn key_err(expected: String, path: &[&str], index: usize) -> ConfigError {
  ConfigError::Key { path: path_str(path, index), expected }
}
fn exist_err(path: &[&str], index: usize) -> ConfigError {
  ConfigError::Exist { path: path_str(path, index) }
}
fn path_err(path: &[&str], index: usize) -> ConfigError {
  ConfigError::Path { path: path_str(path, index) }
}
fn parse_err(path: &[&str], reason: &str, value: String, extra: String) -> ConfigError {
  ConfigError::Parse {
    path: path_str(path, path.len() - 1),
    reason: reason.to_string(),
    value,
    extra,
  }
}
fn type_err(path: &[&str], expected: &str) -> ConfigError {
  ConfigError::Type { path: path_str(path, path.len() - 1), expected: expected.to_string() }
}

// Used to generate the require_*() functions in Config.
macro_rules! must {
  ($v: expr) => {
    match $v {
      Ok(v) => v,
      Err(e) => {
        error!("while parsing yml: {}", e);
        process::exit(1);
      }
    }
  };
}
// Used to get a typed array in Config
macro_rules! get_arr {
  ($self: expr, $path: expr, $type_fn: ident, $type_name: expr) => {{
    let arr = $self.get_arr($path)?;
    let mut new_arr = vec![];
    for (i, e) in arr.iter().enumerate() {
      match e.$type_fn() {
        Some(v) => new_arr.push(v),
        None => {
          let mut vec = $path.to_vec();
          let string = &i.to_string();
          vec.push(string);
          return Err(key_err($type_name, &vec, vec.len() - 1));
        }
      }
    }
    Ok(new_arr)
  }};
}

// Allowing dead code is acceptable here, as the getters for
// the config are not always going to be used.
#[allow(dead_code)]
impl Config {
  pub fn new() -> Self {
    let path = dirs::config_dir().unwrap().join("rebar").join("config.yml");
    if !path.exists() {
      fs::create_dir_all(path.parent().unwrap())
        .expect("Could not create the rebar config directory");
      fs::write(&path, include_bytes!("../../../../sample_config.yml"))
        .expect("Could not write sample config to disk");
    }
    let mut values =
      YamlLoader::load_from_str(&fs::read_to_string(path).expect("Could not read config file"))
        .expect("Invalid yml");
    if values.len() != 1 {
      panic!("Expected a single map as the root node in the config!");
    }
    let conf = values.pop().unwrap();
    if let Some(h) = conf.into_hash() {
      Config { yml: h }
    } else {
      panic!("Expected a map as the root type in the config!");
    }
  }

  pub fn prefixed<'a>(&'a self, prefix: &'a [&'a str]) -> PrefixedConfig<'a> {
    PrefixedConfig::new(self, prefix)
  }

  // This takes a path, and loads the value. If that value is a string, and starts
  // with a '$', then this function gets the value at vars.name. This recursively
  // runs until it gets a value that does not start in a $.
  fn unwrap_var(&self, path: &[&str]) -> Result<&Yaml, ConfigError> {
    let mut path = Vec::from(path);
    loop {
      let val = self.get(&path)?;
      match val {
        Yaml::String(s) => {
          if s.chars().nth(0) == Some('$') {
            path = Vec::from(["vars", &s[1..]]);
          } else {
            return Ok(val);
          }
        }
        _ => return Ok(val),
      }
    }
  }

  pub fn must_int(&self, path: &[&str]) -> i64 {
    must!(self.get_int(path))
  }
  pub fn must_uint(&self, path: &[&str]) -> u64 {
    must!(self.get_uint(path))
  }
  pub fn must_str(&self, path: &[&str]) -> &str {
    must!(self.get_str(path))
  }
  pub fn must_arr(&self, path: &[&str]) -> &Vec<Yaml> {
    must!(self.get_arr(path))
  }
  pub fn must_str_arr(&self, path: &[&str]) -> Vec<&str> {
    must!(self.get_str_arr(path))
  }
  pub fn must_int_arr(&self, path: &[&str]) -> Vec<i64> {
    must!(self.get_int_arr(path))
  }
  pub fn must_col(&self, path: &[&str]) -> Color {
    must!(self.get_col(path))
  }
  pub fn must_fmt(&self, path: &[&str], args: HashMap<String, String>) -> format::Formatted {
    must!(self.get_fmt(path, args))
  }

  pub fn get_int(&self, path: &[&str]) -> Result<i64, ConfigError> {
    match self.unwrap_var(path)? {
      Yaml::Integer(i) => Ok(*i),
      _ => Err(type_err(path, "int")),
    }
  }
  pub fn get_uint(&self, path: &[&str]) -> Result<u64, ConfigError> {
    let v = self.get_int(path)?;
    if v < 0 {
      Err(parse_err(path, "is less than zero", v.to_string(), "expected a positive integer".into()))
    } else {
      Ok(v as u64)
    }
  }
  pub fn get_str(&self, path: &[&str]) -> Result<&str, ConfigError> {
    match self.unwrap_var(path)? {
      Yaml::String(s) => Ok(s),
      _ => Err(type_err(path, "str")),
    }
  }
  pub fn get_arr(&self, path: &[&str]) -> Result<&Vec<Yaml>, ConfigError> {
    match self.unwrap_var(path)? {
      Yaml::Array(a) => Ok(a),
      _ => Err(type_err(path, "arr")),
    }
  }
  pub fn get_str_arr(&self, path: &[&str]) -> Result<Vec<&str>, ConfigError> {
    get_arr!(self, path, as_str, "str".to_string())
  }
  pub fn get_int_arr(&self, path: &[&str]) -> Result<Vec<i64>, ConfigError> {
    get_arr!(self, path, as_i64, "int".to_string())
  }
  pub fn get_col(&self, path: &[&str]) -> Result<Color, ConfigError> {
    color::parse(path, self.get_str(path)?)
  }
  pub fn get_fmt(
    &self,
    path: &[&str],
    args: HashMap<String, String>,
  ) -> Result<format::Formatted, ConfigError> {
    format::parse(path, self.get_str(path)?, args)
  }
  fn get(&self, path: &[&str]) -> Result<&Yaml, ConfigError> {
    if path.len() == 0 {
      Err(path_err(&[], 0))
    } else {
      let map = self.get_map(&path[0..path.len() - 1])?;
      let key = &Yaml::String(path[path.len() - 1].to_string());
      if !map.contains_key(key) {
        return Err(exist_err(path, path.len() - 1));
      }
      Ok(&map[key])
    }
  }
  fn get_map(&self, path: &[&str]) -> Result<&Hash, ConfigError> {
    if path.len() == 0 {
      Ok(&self.yml)
    } else {
      let key = &Yaml::String(path[0].to_string());
      if !self.yml.contains_key(key) {
        return Err(exist_err(path, 0));
      }
      let mut current = match self.yml[key].as_hash() {
        Some(v) => v,
        None => return Err(key_err("map".to_string(), path, 0)),
      };
      for i in 1..path.len() {
        let key = &Yaml::String(path[i].to_string());
        if !current.contains_key(key) {
          return Err(exist_err(path, i));
        }
        current = match current[&Yaml::String(path[i].to_string())].as_hash() {
          Some(v) => v,
          None => return Err(key_err("map".to_string(), path, i)),
        };
      }
      Ok(current)
    }
  }
}

use super::*;

pub struct PrefixedConfig<'a> {
  prefix: &'a [&'a str],
  inner: &'a Config,
}

macro_rules! impl_fn {
  ($fn: ident, $ret: ty) => {
    pub fn $fn(&self, path: &[&str]) -> $ret {
      self.inner.$fn(&[self.prefix, path].concat())
    }
  };
}

impl<'a> PrefixedConfig<'a> {
  pub fn new(config: &'a Config, prefix: &'a [&'a str]) -> Self {
    PrefixedConfig { prefix, inner: config }
  }
  impl_fn!(get_int, Result<i64, ConfigError>);
  impl_fn!(get_uint, Result<u64, ConfigError>);
  impl_fn!(get_str, Result<&str, ConfigError>);
  impl_fn!(get_arr, Result<&Vec<Yaml>, ConfigError>);
  impl_fn!(get_str_arr, Result<Vec<&str>, ConfigError>);
  impl_fn!(get_int_arr, Result<Vec<i64>, ConfigError>);
  impl_fn!(get_col, Result<Color, ConfigError>);
  pub fn get_fmt(
    &self,
    path: &[&str],
    args: HashMap<String, String>,
  ) -> Result<format::Formatted, ConfigError> {
    self.inner.get_fmt(&[self.prefix, path].concat(), args)
  }

  impl_fn!(must_int, i64);
  impl_fn!(must_uint, u64);
  impl_fn!(must_str, &str);
  impl_fn!(must_arr, &Vec<Yaml>);
  impl_fn!(must_str_arr, Vec<&str>);
  impl_fn!(must_int_arr, Vec<i64>);
  impl_fn!(must_col, Color);
  pub fn must_fmt(&self, path: &[&str], args: HashMap<String, String>) -> format::Formatted {
    self.inner.must_fmt(&[self.prefix, path].concat(), args)
  }
}

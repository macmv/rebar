#![feature(test)]

extern crate test;

pub mod config;

use crate::config::PrefixedConfig;
use std::{fmt, io, sync::mpsc};

pub use crate::config::Config;

pub trait Module {
  /// This should do things like spawn threads to call Redraw() for this module.
  /// If this function does nothing, then this module will never be redrawn.
  fn init(&self, data: ModuleData, config: PrefixedConfig);
  /// Called any time the module should be re-rendered. This
  /// should update an internal Buffer. Returns the width of
  /// the module after rendering.
  fn render(&self, buf: &Box<dyn Buffer>, config: PrefixedConfig) -> u32;
  /// The name of the module. This should be the name that
  /// the config file uses.
  fn name(&self) -> &'static str;
}

/// Side of the bar that a module is on. In the future, Center may be added.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Side {
  Left,
  Right,
}

/// The position of a module. This is used to tell the bar where are a module
/// is. See [`EventKind::Redraw`] for an example.
#[derive(Clone, Copy, Debug)]
pub struct ModulePos {
  side: Side,
  index: usize,
}

impl ModulePos {
  /// Creates a new module position. Index should be a valid index within a
  /// bar's list of modules. This should only be used within the bar. Generally,
  /// it is not a good idea to create one of these.
  pub fn new(side: Side, index: usize) -> ModulePos {
    ModulePos { side: side, index: index }
  }
  /// Returns the side that this position is on.
  #[inline(always)]
  pub fn side(&self) -> Side {
    self.side
  }
  /// Returns the index that this position points to.
  #[inline(always)]
  pub fn index(&self) -> usize {
    self.index
  }
}

/// Any event sent to the backend. This has a bar index, and an event kind.
/// The bar index is used within the backend to get the correct bar, which
/// will then pass the event to [`Bar::handle_event`].
#[derive(Clone, Copy, Debug)]
pub struct BarEvent {
  // An index within the backend's list of bars.
  pub bar: usize,
  pub kind: EventKind,
}

/// An event that the bar must handle.
#[derive(Clone, Copy, Debug)]
pub enum EventKind {
  /// Redraws a single module. This will also move other modules around
  /// if the width of this module changes.
  Redraw(ModulePos),
  /// Redraws all modules. Currently, this is only used when the backend
  /// gets a window expose event.
  RedrawAll(),
}

/// This is a handle for any module specific data. It contains the position
/// of the module, and a [`mpsc::Sender`] to send messages back to the bar.
/// This should be passed to any new threads, as Bar is not Send.
#[derive(Clone)]
pub struct ModuleData {
  // An index within the backend's list of bars.
  bar: usize,
  output: String,
  pos: ModulePos,
  tx: mpsc::Sender<BarEvent>,
}

impl ModuleData {
  /// Creates a new module. bar is an index into the backend's list of bars. pos
  /// is the position of the module that this data is for. tx is linked to the
  /// backend, so that this ModuleData can send messages back to the main
  /// thread. This should probably never be called outside of bar.
  pub fn new(bar: usize, output: String, pos: ModulePos, tx: mpsc::Sender<BarEvent>) -> ModuleData {
    ModuleData { bar, output, pos, tx }
  }
  /// Sends an event to the bar, requesting that this module be redrawn. Events
  /// are handled within the backend, so as long as there aren't a bunch of
  /// other events, this will redraw the module almost immediately.
  pub fn redraw(&self) {
    self.tx.send(BarEvent { bar: self.bar, kind: EventKind::Redraw(self.pos) }).unwrap();
  }
  /// Returns the output that this bar is on.
  pub fn output(&self) -> String {
    self.output.clone()
  }
}

/// This is a region within the bar. It can be resized and
/// rendered onto the window. Usually, one of these is created
/// for each module within the bar. If a module needs to render
/// to multiple buffers (for whatever reason) it can request a
/// new buffer from the bar. This type also contains all of the
/// drawing primitives used for bars.
pub trait Buffer {
  /// Draws a rectangle to the bar. The position is relative to
  /// the buffer's position. The coordinates are always absolute,
  /// so a vertical bar will need some changes to width/height.
  fn draw_rect(&self, color: Color, x: i32, y: i32, width: u32, height: u32);
  /// Fills the entire buffer with the given color.
  fn clear_color(&self, color: Color) {
    self.draw_rect(color, 0, 0, self.width(), self.height());
  }
  /// Fills the entire buffer with the background color.
  fn clear(&self) {
    self.draw_rect(self.get_background(), 0, 0, self.width(), self.height());
  }
  /// Draws some text onto the bar. This will be scaled according
  /// to your config. Returns the width of the text drawn.
  fn draw_text(&self, color: Color, x: i32, y: i32, text: &str) -> u32;
  /// Resizes the buffer. Any blank pixels should be filled with the
  /// background color.
  fn resize(&mut self, width: u32, height: u32);
  /// Sets the position of the buffer. This is only used when the buffer
  /// is flushed.
  fn set_pos(&mut self, x: i32, y: i32);
  /// Sets the background color. Most functions need a background color,
  /// but it is usually the same color. So, I have opted to make the background
  /// a field of the buffer. If you change this between calls, it will act
  /// as you expect. That is, it will not redraw previous objects with
  /// the new background.
  fn set_background(&mut self, color: Color);
  /// Gets the buffer's current background color. See `set_background()` for
  /// more.
  fn get_background(&self) -> Color;
  /// Returns the x position of this module. If this buffer is not going
  /// to be flushed, then this value will be ignored.
  fn x(&self) -> i32;
  /// Returns the y position of this module. If this buffer is not going
  /// to be flushed, then this value will be ignored.
  fn y(&self) -> i32;
  /// Returns the width of this module.
  fn width(&self) -> u32;
  /// Returns the height of this module.
  fn height(&self) -> u32;
  /// Flushes this buffer to the window. This will copy all data
  /// within the buffer onto the underlying window.
  fn flush(&self);
}

/// A window used by the bar. This will wrap an X11 or Wayland
/// window.
pub trait Window {
  /// Creates a new buffer. This is a pixmap on X11, and an shm buffer on
  /// Wayland. Buffers are used for every bar module, and to cache font glyphs.
  /// X and Y are only used on buffer.flush().
  fn create_buffer(&self, x: i32, y: i32, width: u32, height: u32) -> Box<dyn Buffer>;
  /// Width of the entire window.
  fn width(&self) -> u32;
  /// Height of the entire window.
  fn height(&self) -> u32;
  /// The name of the monitor this window is on. Should match a monitor name on
  /// i3/sway.
  fn name(&self) -> String;
}

/// This is a generic color type. It is stored in the format ARGB.
/// This does not implement From<u32>, as colors should be declared
/// explicitly with [`Color::from_u32()`].
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Color(u32);

#[allow(unused)]
impl Color {
  /// Creates a new color from the given alpha, red, green and blue values.
  pub fn from_argb(a: u8, r: u8, g: u8, b: u8) -> Self {
    Color(((a as u32) << 24) | ((r as u32) << 16) | ((g as u32) << 8) | b as u32)
  }
  /// Creates a new color from the given alpha, red, green and blue values.
  /// Unline from_argb(), this function takes floats from 0 to 1, instead
  /// of ints from 0 to 255.
  pub fn from_argbf(a: f64, r: f64, g: f64, b: f64) -> Self {
    Color::from_argb(
      (a * 255.0).round() as u8,
      (r * 255.0).round() as u8,
      (g * 255.0).round() as u8,
      (b * 255.0).round() as u8,
    )
  }
  /// Creates a new color with the one value as r, g, and b. Alpha will be 255.
  pub fn from_grayscale(c: u8) -> Self {
    Color::from_argb(255, c, c, c)
  }
  /// Returns a color that is interpolated between a and b. The closer that
  /// v is to 0, the closer that the color is to a. The closer that v is
  /// to 1, the closer the color is to b. Any value below 0 or above 1
  /// will be capped to just return a or b.
  pub fn lerp(a: Color, b: Color, v: f64) -> Self {
    if v <= 0.0 {
      return a;
    } else if v >= 1.0 {
      return b;
    }
    Color::from_argbf(
      (b.af() - a.af()) * v + a.af(),
      (b.rf() - a.rf()) * v + a.rf(),
      (b.gf() - a.gf()) * v + a.gf(),
      (b.bf() - a.bf()) * v + a.bf(),
    )
  }
  /// Returns the alpha of this color.
  #[inline(always)]
  pub fn a(&self) -> u8 {
    (self.0 >> 24) as u8
  }
  /// Returns the red value of this color.
  #[inline(always)]
  pub fn r(&self) -> u8 {
    (self.0 >> 16) as u8
  }
  /// Returns the green value of this color.
  #[inline(always)]
  pub fn g(&self) -> u8 {
    (self.0 >> 8) as u8
  }
  /// Returns the blue value of this color.
  #[inline(always)]
  pub fn b(&self) -> u8 {
    self.0 as u8
  }
  /// Returns the alpha of this color, from 0 to 1.
  #[inline(always)]
  pub fn af(&self) -> f64 {
    self.a() as f64 / 255.0
  }
  /// Returns the red value of this color, from 0 to 1.
  #[inline(always)]
  pub fn rf(&self) -> f64 {
    self.r() as f64 / 255.0
  }
  /// Returns the green value of this color, from 0 to 1.
  #[inline(always)]
  pub fn gf(&self) -> f64 {
    self.g() as f64 / 255.0
  }
  /// Returns the blue value of this color, from 0 to 1.
  #[inline(always)]
  pub fn bf(&self) -> f64 {
    self.b() as f64 / 255.0
  }

  /// Creates a new color from a u32. This int should be in the format
  /// `0xaarrggbb`.
  pub fn from_u32(val: u32) -> Self {
    Color(val)
  }
  /// Reads four bytes from the given [`io::Read`], and makes a color from those
  /// four bytes. The bytes should be alpha, then red, green, and blue.
  pub fn read_bytes<R: io::Read>(reader: &mut R) -> Self {
    let mut buf: [u8; 4] = [0; 4];
    reader.read(&mut buf).unwrap();
    Color(u32::from_ne_bytes(buf))
  }
  /// Returns the internal u32 that this color is. The u32 will be in
  /// the format `0xaarrggbb`.
  pub fn as_u32(&self) -> u32 {
    self.0
  }
  /// Returns an [u8], in the order `[a, r, g, b]`. Used in wayland
  /// to write to a buffer.
  pub fn as_bytes(&self) -> [u8; 4] {
    self.0.to_ne_bytes()
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use test::Bencher;

  #[test]
  fn from_argbf() {
    let a = Color::from_argb(128, 64, 255, 0);
    let b = Color::from_argbf(0.5, 0.25, 1.0, 0.0);
    assert_eq!(a, b);
  }

  #[test]
  fn lerp() {
    let a = Color::from_argb(0, 64, 128, 50);
    let b = Color::from_argb(255, 128, 255, 100);
    assert_eq!(Color::lerp(a, b, 0.0), a);
    assert_eq!(Color::lerp(a, b, 1.0), b);

    let a = Color::from_argb(0, 50, 100, 100);
    let b = Color::from_argb(10, 60, 200, 0);
    for i in 0..10 {
      assert_eq!(
        Color::lerp(a, b, i as f64 / 10.0),
        Color::from_argb(i, i + 50, i * 10 + 100, 100 - i * 10),
        "index: {}",
        i
      );
    }
  }

  #[bench]
  fn bench_lerp(b: &mut Bencher) {
    b.iter(|| {
      let c1 = Color::from_argb(0, 64, 128, 50);
      let c2 = Color::from_argb(255, 128, 255, 100);
      for i in 0..10 {
        let v = test::black_box(i as f64 / 10.0);
        test::black_box(Color::lerp(c1, c2, v));
      }
    });
  }
}

impl fmt::Debug for Color {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "Color(0x{:08x})", self.0)
  }
}

pub mod mpd;
pub mod sway;
pub mod sys;

use common::config;

// Avoids changing the function name in the library
#[no_mangle]
pub fn get_modules() -> Vec<Box<dyn common::Module>> {
  println!("Hello from library!");
  println!("Returning module");
  vec![
    Box::new(mpd::Module::new()),
    // Box::new(sway::Module::new()),
    Box::new(sys::FS::new()),
    Box::new(sys::CPU::new()),
    Box::new(sys::Mem::new()),
  ]
}

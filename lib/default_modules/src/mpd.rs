extern crate mpd;

use common;
use common::{config, ModuleData};

use std::{cell::RefCell, collections::HashMap};

pub struct Module {
  conn: RefCell<mpd::Client>,
}

impl common::Module for Module {
  fn init(&self, data: ModuleData, conf: config::PrefixedConfig) {}
  fn render(&self, buf: &Box<dyn common::Buffer>, conf: config::PrefixedConfig) -> u32 {
    let pad = 20;
    let mut conn = self.conn.borrow_mut();

    let mut args = HashMap::new();
    args.insert("prev".into(), conf.must_str(&["prev-text"]).into());
    args.insert("next".into(), conf.must_str(&["next-text"]).into());

    let status = conn.status().unwrap();
    args.insert(
      "play".into(),
      match status.state {
        mpd::State::Play => conf.must_str(&["play-text"]).into(),
        mpd::State::Pause => conf.must_str(&["pause-text"]).into(),
        mpd::State::Stop => conf.must_str(&["pause-text"]).into(),
      },
    );

    match conn.currentsong().unwrap() {
      Some(song) => {
        args.insert("title".into(), song.file);
        let (current, total) = status.time.unwrap();
        args.insert(
          "current-time".into(),
          format!("{:01}:{:02}", current.num_minutes(), current.num_seconds() % 60),
        );
        args.insert(
          "total-time".into(),
          format!("{:01}:{:02}", total.num_minutes(), total.num_seconds() % 60),
        );
      }
      None => {
        args.insert("title".into(), "None".to_string());
        args.insert("current-time".into(), "0:00".to_string());
        args.insert("total-time".into(), "0:00".to_string());
      }
    }

    let width =
      buf.draw_text(conf.must_col(&["text-color"]), pad, 35, &conf.must_fmt(&["fmt"], args).text);
    buf.draw_rect(conf.must_col(&["underline-color"]), pad, 45, width, 5);
    pad as u32 * 2 + width
  }
  fn name(&self) -> &'static str {
    "mpd"
  }
}

impl Module {
  pub fn new() -> Self {
    Module { conn: RefCell::new(mpd::Client::connect("127.0.0.1:6600").unwrap()) }
  }
}

extern crate i3ipc;

use crate::config;
use common;
use common::ModuleData;

use i3ipc::event;
use std::{
  collections::HashMap,
  sync::{Arc, Mutex},
  thread,
};

pub struct Module {
  output: String,

  conn: Arc<Mutex<i3ipc::I3Connection>>,
  workspaces: Arc<Mutex<Vec<i3ipc::reply::Workspace>>>,
}

impl common::Module for Module {
  fn init(&self, data: ModuleData, conf: config::PrefixedConfig) {
    // Update buttons in the future
    let workspaces = self.workspaces.clone();
    let conn = self.conn.clone();
    let output = self.output.clone();
    thread::spawn(move || {
      let mut events =
        i3ipc::I3EventListener::connect().expect("Could not open connection to I3 events!");
      events.subscribe(&[i3ipc::Subscription::Workspace]).unwrap();
      for e in events.listen() {
        let e = e.unwrap();
        match e {
          event::Event::WorkspaceEvent(e) => {
            {
              let nodes =
                conn.lock().unwrap().get_workspaces().expect("could not get workspaces").workspaces;
              let mut workspaces = workspaces.lock().unwrap();
              workspaces.clear();
              for w in nodes {
                if w.output != output {
                  continue;
                }
                workspaces.push(w);
              }
            }
            data.redraw();
          }
          _ => {
            println!("Got unknown event: {:?}", e);
          }
        }
      }
    });
  }
  fn render(&self, buf: &Box<dyn common::Buffer>, conf: config::PrefixedConfig) -> u32 {
    let mut x = 20;
    let workspaces = self.workspaces.lock().unwrap();

    for w in workspaces.iter() {
      let mut args = HashMap::new();
      args.insert("name".into(), w.name.clone());

      let width =
        buf.draw_text(conf.must_col(&["text-color"]), x, 35, &conf.must_fmt(&["fmt"], args).text);
      if w.focused {
        buf.draw_rect(conf.must_col(&["underline-color"]), x - 10, 45, width + 20, 5);
      }
      x += width as i32 + 40;
    }
    x -= 20;
    x as u32
  }
  fn name(&self) -> &'static str {
    "sway"
  }
}

impl Module {
  pub fn new() -> Self {
    // Sway also uses I3SOCK, so this is easy
    let module = Module {
      // output: data.output().to_string(),
      output: "DP-0".into(),

      conn: Arc::new(Mutex::new(i3ipc::I3Connection::connect().unwrap())),
      workspaces: Arc::new(Mutex::new(vec![])),
    };
    // Create inital buttons
    {
      let nodes =
        module.conn.lock().unwrap().get_workspaces().expect("Could not get workspaces!").workspaces;
      let mut workspaces = module.workspaces.lock().unwrap();
      for w in nodes {
        if w.output != module.output {
          continue;
        }
        workspaces.push(w);
      }
    }
    module
  }
}

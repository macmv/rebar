extern crate systemstat;

use crate::config;
use common;
use common::ModuleData;

use std::{
  collections::HashMap,
  sync::{Arc, Mutex},
  thread,
  time::Duration,
};
use systemstat::{CPULoad, Platform};
use yaml_rust::Yaml;

pub struct FS {}

impl common::Module for FS {
  fn init(&self, data: ModuleData, conf: config::PrefixedConfig) {
    let refresh = Duration::from_secs(conf.must_uint(&["refresh"]));
    let data = data.clone();
    thread::spawn(move || loop {
      thread::sleep(refresh);
      data.redraw();
    });
  }
  fn render(&self, buf: &Box<dyn common::Buffer>, conf: config::PrefixedConfig) -> u32 {
    let pad = 20;

    let mut args = HashMap::new();
    // This is not done on load, because anytime the module is redrawn, it should
    // poll the filesystem again. This module will probably only redraw itselt
    // once every five minutes or so, so it doesn't end up being a problem.
    for y in conf.must_arr(&["drives"]) {
      // TODO: Proper error handling
      let map = y.as_hash().unwrap();
      let name = map[&Yaml::String("name".into())].as_str().unwrap();
      let path = map[&Yaml::String("path".into())].as_str().unwrap();

      let stats = systemstat::System::new().mount_at(path).unwrap();
      // Total filesystem size.
      let total = stats.total.0;
      // Amount free in bytes. Not all bytes are usable, because of journalling.
      let free = stats.free.0;
      // Amount of avalible space. All of this space is usable.
      let available = stats.avail.0;
      // Amount of used space.
      let used = total - available;

      // Percentage usage
      args.insert(
        name.to_string() + ":usage",
        ((used as f64 / total as f64 * 100.0) as u32).to_string(),
      );
      args.insert(name.to_string() + ":total", fs_str(total));
      args.insert(name.to_string() + ":free", fs_str(free));
      args.insert(name.to_string() + ":available", fs_str(available));
      args.insert(name.to_string() + ":used", fs_str(used));
    }

    let width =
      buf.draw_text(conf.must_col(&["text-color"]), pad, 35, &conf.must_fmt(&["fmt"], args).text);
    buf.draw_rect(conf.must_col(&["underline-color"]), pad, 45, width, 5);
    pad as u32 * 2 + width
  }
  fn name(&self) -> &'static str {
    "fs"
  }
}

// These are all in GiB, which is a much better way of working
// with files than something like GaB. The difference is GaB
// is usually just spelled GB, and it is about powers of 1000,
// instead of powers of 1024.
fn fs_str(amount: u64) -> String {
  if amount > 1 << 40 {
    format!("{:.2}TB", amount as f64 / (1u64 << 40) as f64)
  } else if amount > 1 << 30 {
    format!("{:.2}GB", amount as f64 / (1u64 << 30) as f64)
  } else if amount > 1 << 20 {
    format!("{:.2}MB", amount as f64 / (1u64 << 20) as f64)
  } else if amount > 1 << 10 {
    format!("{:.2}KB", amount as f64 / (1u64 << 10) as f64)
  } else {
    format!("{}B", amount)
  }
}

impl FS {
  pub fn new() -> Self {
    FS {}
  }
}

pub struct CPU {
  load: Arc<Mutex<Option<CPULoad>>>,
  // output: String,
}

impl common::Module for CPU {
  fn init(&self, data: ModuleData, conf: config::PrefixedConfig) {
    let load = self.load.clone();
    let sys = systemstat::System::new();
    let refresh = Duration::from_secs(conf.must_uint(&["refresh"]));
    thread::spawn(move || loop {
      let cpu_load = sys.cpu_load().unwrap();
      thread::sleep(refresh);
      let cpu_load = cpu_load.done().unwrap();
      let mut value = cpu_load[0].clone();
      for l in cpu_load.iter().skip(1) {
        value = value.avg_add(l);
      }
      {
        let mut load = load.lock().unwrap();
        *load = Some(value);
      }
      data.redraw();
    });
  }
  fn render(&self, buf: &Box<dyn common::Buffer>, conf: config::PrefixedConfig) -> u32 {
    let pad = 20;

    let mut args = HashMap::new();
    let load = self.load.lock().unwrap();
    match &*load {
      Some(l) => {
        args.insert("user".into(), (l.user * 100.0).to_string());
        args.insert("nice".into(), (l.nice * 100.0).to_string());
        args.insert("sys".into(), (l.system * 100.0).to_string());
        args.insert("interrupt".into(), (l.interrupt * 100.0).to_string());
        args.insert("idle".into(), (l.idle * 100.0).to_string());
        args.insert(
          "total".into(),
          format!("{:.00}", (l.user + l.nice + l.system + l.interrupt) * 100.0),
        );
      }
      None => {
        args.insert("user".into(), "0".into());
        args.insert("nice".into(), "0".into());
        args.insert("sys".into(), "0".into());
        args.insert("interrupt".into(), "0".into());
        args.insert("idle".into(), "0".into());
        args.insert("total".into(), "0".into());
      }
    }

    let width =
      buf.draw_text(conf.must_col(&["text-color"]), pad, 35, &conf.must_fmt(&["fmt"], args).text);
    buf.draw_rect(conf.must_col(&["underline-color"]), pad, 45, width, 5);
    pad as u32 * 2 + width
  }
  fn name(&self) -> &'static str {
    "cpu"
  }
}

impl CPU {
  pub fn new() -> Self {
    let load = Arc::new(Mutex::new(None));
    CPU { load }
  }
}

pub struct Mem {}

impl common::Module for Mem {
  fn init(&self, data: ModuleData, conf: config::PrefixedConfig) {
    let refresh = Duration::from_secs(conf.must_uint(&["refresh"]));
    thread::spawn(move || loop {
      thread::sleep(refresh);
      data.redraw();
    });
  }
  fn render(&self, buf: &Box<dyn common::Buffer>, conf: config::PrefixedConfig) -> u32 {
    let pad = 20;

    let mut args = HashMap::new();
    let mem = systemstat::System::new().memory().unwrap();
    args.insert("total".into(), fs_str(mem.total.0));
    args.insert("free".into(), fs_str(mem.free.0));
    args.insert("used".into(), fs_str(mem.total.0 - mem.free.0));
    args.insert(
      "usage".into(),
      format!("{:.00}", (1.0 - mem.free.0 as f64 / mem.total.0 as f64) * 100.0),
    );

    let width =
      buf.draw_text(conf.must_col(&["text-color"]), pad, 35, &conf.must_fmt(&["fmt"], args).text);
    buf.draw_rect(conf.must_col(&["underline-color"]), pad, 45, width, 5);
    pad as u32 * 2 + width
  }
  fn name(&self) -> &'static str {
    "mem"
  }
}

impl Mem {
  pub fn new() -> Self {
    Mem {}
  }
}

pub mod shared;

use common::{BarEvent, Config, EventKind, Module, ModuleData, ModulePos, Side};
use log::{error, warn};
use std::{
  collections::HashMap,
  sync::{mpsc, Arc},
};

/// A bar on the top of the screen. This wraps a window, and manages all
/// of the modules that are rendered. It has a [`Config`], which is used to
/// create and configure all [`Module`]s.
#[allow(unused)]
pub struct Bar {
  left: Vec<ModuleBuffer>,
  right: Vec<ModuleBuffer>,
  win: Box<dyn common::Window>,
  config: Arc<Config>,
  tx: mpsc::Sender<BarEvent>,
  index: usize,
  background: common::Color,
}

/// This is a module and a buffer. It is not Send, and is stored in the [`Bar`].
/// This can be accesed with a [`ModuleData`] that points to the correct index.
struct ModuleBuffer {
  buf: Box<dyn common::Buffer>,
  module: Box<dyn Module>,
}

impl Bar {
  pub fn new(
    libs: &Vec<shared::ModuleLibrary>,
    win: Box<dyn common::Window>,
    config: Arc<Config>,
    tx: mpsc::Sender<BarEvent>,
    index: usize,
  ) -> Self {
    let mut bar = Bar {
      left: vec![],
      right: vec![],
      background: config.must_col(&["bars", &win.name(), "background"]),
      win,
      config,
      tx,
      index,
    };

    bar.create_modules_list(libs, Side::Left);
    bar.create_modules_list(libs, Side::Right);
    bar
  }
  fn create_modules_list(&mut self, libs: &Vec<shared::ModuleLibrary>, side: Side) {
    let name;
    match side {
      Side::Left => name = "left",
      Side::Right => name = "right",
    }
    let names = self.config.must_str_arr(&["bars", &self.win.name(), "modules", name]);

    let mut modules = vec![];
    for n in names {
      // This should try to create a new module with the given name.
      match lib.get_module(n) {
        Some(m) => {
          let pos = ModulePos::new(side, modules.len());
          modules.push(self.new_module(m, pos));
        }
        None => {
          warn!("Unknown module: {}", n);
        }
      }
    }
    match side {
      Side::Left => self.left = modules,
      Side::Right => self.right = modules,
    }
  }
  fn new_data(&self, pos: ModulePos) -> ModuleData {
    ModuleData::new(self.index, self.win.name(), pos, self.tx.clone())
  }
  fn new_module(&self, module: Box<dyn Module>, pos: ModulePos) -> ModuleBuffer {
    let data = self.new_data(pos);
    module.init(data, self.config.prefixed(&[module.name()]));
    ModuleBuffer { module, buf: self.create_buffer(pos.side()) }
  }
  /// Processes an event from another module. This will be called from
  /// the main thread, so that modules can poll for events on another thread,
  /// and then render on the main thread.
  pub(crate) fn handle_event(&mut self, e: &BarEvent) {
    match e.kind {
      EventKind::Redraw(pos) => self.render(pos),
      EventKind::RedrawAll() => {
        for i in 0..self.left.len() {
          self.render(ModulePos::new(Side::Left, i));
        }
        for i in 0..self.right.len() {
          self.render(ModulePos::new(Side::Right, i));
        }
      }
    }
  }
  fn render(&mut self, pos: ModulePos) {
    let list = match pos.side() {
      Side::Left => &mut self.left,
      Side::Right => &mut self.right,
    };
    if pos.index() < 0 || pos.index() >= list.len() {
      error!(
        "While rendering a module, got a module pos out of range. Expected 0 < x < {}, got x = {}",
        list.len(),
        pos.index()
      );
      return;
    }
    let module = &mut list[pos.index()];
    // TODO: Modules should have a custom background that is implemented
    // in the bar.
    module.buf.set_background(self.background);
    if module.render(&self.config) {
      // Means the module changed size
      // This is the edge of the module, and will
      // be changed as the rest of the modules are
      // moved over.
      match pos.side() {
        Side::Left => {
          // The new rightmost edge of the left side.
          let mut edge = module.buf.x() + module.buf.width() as i32;
          for m in list[pos.index() + 1..].iter_mut() {
            // Now we move through each module and align it with each edge.
            m.buf.set_pos(edge, 0);
            m.buf.flush();
            edge += m.buf.width() as i32;
          }
        }
        Side::Right => {
          // This is the left edge of the module one to the right of pos.index.
          // If pos.index is the rightmost module, then the edge is the width of
          // the monitor.
          let mut edge = if pos.index() + 1 >= list.len() {
            self.win.width() as i32
          } else {
            list[pos.index() + 1].buf.x()
          };
          // We now iterate towards the left of the screen, or towards to the 0 index.
          for m in list[..pos.index() + 1].iter_mut().rev() {
            // Move the edge over
            edge -= m.buf.width() as i32;
            // And finally move this buffer to align with the one to the right
            m.buf.set_pos(edge, 0);
            m.buf.flush();
          }
        }
      };
    }
  }
  /// Returns a cloned Arc<Config>. This can be used to get any fields
  /// from the yml config file.
  pub fn get_config(&self) -> Arc<Config> {
    self.config.clone()
  }
  /// Unlike [`common::Window::create_buffer`], this function will make the
  /// buffer fit the bar. If the bar is horizontal, then size is the width of
  /// the buffer. Otherwise, size is the height of the buffer. The other
  /// dimension will cover then entire width/height of the bar.
  pub fn create_buffer(&self, side: Side) -> Box<dyn common::Buffer> {
    let size = 2000;
    let pos = match side {
      Side::Left => 0,
      Side::Right => self.win.width() as i32,
    };
    // TODO: The bar should know if it is horizontal/vertical, and
    // should calculate the buffer size accordingly.
    self.win.create_buffer(pos, 0, size, self.win.height())
  }
}

impl ModuleBuffer {
  // Returns true if the buffer was resized.
  pub fn render(&mut self, config: &Config) -> bool {
    self.buf.clear();
    let width = self.module.render(&self.buf, config.prefixed(&[self.module.name()]));
    let mut ret = false;
    if width != self.buf.width() {
      self.buf.resize(width, self.buf.height());
      ret = true;
    }
    self.buf.flush();
    ret
  }
}

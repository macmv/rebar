use log::{error, info};
use std::{fs, path::Path};

pub struct ModuleLibrary {
  pub modules: Vec<Box<dyn common::Module>>,
  path: String,
  // Fields are dropped the order that they were declared. So as long as the lib is the last
  // declared field, it will always be dropped last.
  lib: libloading::Library,
}

impl ModuleLibrary {
  pub fn name(&self) -> &str {
    let name = Path::new(&self.path).file_stem().unwrap().to_str().unwrap();
    // Most .so files have a name like: libdefault.so. When this happens, we want to
    // just refer to it as "default".
    if name.starts_with("lib") {
      &name[3..]
    } else {
      name
    }
  }
}

/// Loads all .so files in ~/.config/rebar/modules/. If any of these files do
/// not have the correct symbols, this will exit.
pub fn load() -> Vec<ModuleLibrary> {
  let path = dirs::config_dir().unwrap().join("rebar").join("modules");
  if !path.exists() {
    fs::create_dir_all(&path).expect("Could not create the rebar modules directory");
  }
  let mut libs = vec![];
  for f in fs::read_dir(path).unwrap() {
    let f = f.unwrap();
    match f.path().as_path().extension() {
      Some(p) => {
        if p != "so" {
          continue;
        }
      }
      None => continue,
    }
    info!("Loading {:?} from modules directory...", f.path());
    match get_modules(&f.path().as_path()) {
      Err(e) => error!("Error while loading {:?}: {}", f.path(), e),
      Ok(lib) => {
        libs.push(lib);
      }
    }
  }
  libs
}

fn get_modules(f: &Path) -> Result<ModuleLibrary, libloading::Error> {
  let mut lib;
  // SAFETY: This is not very safe. Assuming that the path we have given is to a
  // valid .so file, and that so file has the correct symbols, then this is safe.
  // There isn't really a way to make this any safer. As for the ModuleLibrary, it
  // will drop everything correctly, so that once this function returns,
  // everything is sound.
  unsafe {
    lib = ModuleLibrary {
      modules: vec![],
      path: f.to_str().unwrap().into(),
      lib: libloading::Library::new(f)?,
    };
    // We assume these .so files are compiled from rust. If you would like to make a
    // module in C (or any other language), you should write a wrapper layer in rust
    // which will call C functions. I couldn't be bothered to mess with pointers
    // and manually setting length and such.
    let func: libloading::Symbol<unsafe fn() -> Vec<Box<dyn common::Module>>> =
      lib.lib.get(b"get_modules")?;
    lib.modules = func();
  }
  Ok(lib)
}

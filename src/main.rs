#![feature(test)]

mod bar;
mod wayland;
mod x11;

use common::{BarEvent, Config, EventKind};
use rusttype::Font;
use simple_logger::SimpleLogger;
use std::sync::{mpsc, Arc};

fn main() {
  SimpleLogger::new().init().unwrap();
  let config = Arc::new(Config::new());

  // Include font data in binary
  let font_data = include_bytes!(
    "/usr/share/fonts/nerd-fonts-complete/TTF/Noto Sans Regular Nerd Font Complete.ttf"
  );
  let font = Font::try_from_bytes(font_data).expect("Could not load font!");
  // Libs must stay in scope for the whole program!
  let libs = bar::shared::load();

  let (tx, rx) = mpsc::channel();
  if wayland::active() {
    println!("Running on a wayland backend");
    let mut backend = wayland::Backend::new();

    for () in backend.monitors() {
      let win = wayland::Window::new(&backend, font.clone());

      let index = 0;
      let _bar = bar::Bar::new(&libs, Box::new(win.clone()), config.clone(), tx.clone(), index);

      let tx = tx.clone();
      win.init(move || {
        println!("Got expose!");
        tx.send(BarEvent { bar: index, kind: EventKind::RedrawAll() }).unwrap();
      });

      backend.add_win(win);
    }
    backend.run();
  } else {
    println!("Running on an X11 backend");
    let mut backend = x11::Backend::new(font, rx).expect("Could not create X backend!");
    let mut index = 0;
    let mut bars = vec![];
    for data in backend.windows.iter_mut() {
      let bar = bar::Bar::new(&libs, Box::new(data.win.clone()), config.clone(), tx.clone(), index);

      // bar.run();
      let tx = tx.clone();
      data.on_expose = Some(Box::new(move || {
        println!("Got expose!");
        tx.send(BarEvent { bar: index, kind: EventKind::RedrawAll() }).unwrap();
      }));

      bars.push(bar);
      index += 1;
    }
    backend.set_bars(bars);
    backend.run();
  }

  // let app = gtk::Application::new(Some("macmv.rust-bar"), Default::default())
  //   .expect("Failed to start gtk app");
  //
  // app.connect_activate(|app| {
  //   let win = gtk::ApplicationWindow::new(app);
  //
  //   let container = gtk::Box::new(gtk::Orientation::Horizontal, 10);
  //   win.add(&container);
  //
  // });
  //
  // app.run(&[]);
}

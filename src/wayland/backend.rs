use crate::wayland::window::Window;

use std::{cell::RefCell, rc::Rc};
use wayland_client::{
  protocol::{wl_compositor, wl_output, wl_shm},
  Display, EventQueue, GlobalManager, Main,
};
use wayland_protocols::wlr::unstable::layer_shell::v1::client::zwlr_layer_shell_v1;

pub struct Backend {
  globals: GlobalManager,
  pub(crate) data: Rc<RefCell<BackendData>>,
  // window_ids: HashMap<xcb::Window, usize>,
  pub(crate) windows: Vec<Window>,
}

pub(crate) struct BackendData {
  pub(crate) queue: EventQueue,

  pub(crate) shm: Main<wl_shm::WlShm>,
  pub(crate) compositor: Main<wl_compositor::WlCompositor>,
  pub(crate) zwlr_shell: Main<zwlr_layer_shell_v1::ZwlrLayerShellV1>,
}

/// With wayland backends, the window needs the callback function before
/// it can be created. This means that the user must iterate through all
/// monitors, and manually add each window. This is fine, it just means
/// there is some different code in main() for the two backends.
impl Backend {
  pub fn new() -> Self {
    let display = Display::connect_to_env().expect("Could not connect to wayland!");
    let mut queue = display.create_event_queue();
    let attached = display.attach(queue.token());
    let globals = GlobalManager::new(&attached);

    // Syncs everything. This will populate globals.
    queue.sync_roundtrip(&mut (), |_, _, _| unreachable!()).unwrap();

    let data = Rc::new(RefCell::new(BackendData {
      // display: attached,
      queue: queue,
      // globals.instantiate_exact uses generic types, so each call
      // will return something different. The number is the version
      // that we want from the wayland compositor.
      shm: globals.instantiate_exact(1).unwrap(),
      compositor: globals.instantiate_exact(1).unwrap(),
      zwlr_shell: globals.instantiate_exact(2).unwrap(),
    }));

    Backend {
      data: data,
      globals: globals,
      windows: vec![],
      /* compositor: globals.instantiate_exact::<wl_compositor::WlCompositor>(1).unwrap(),
       * shell: globals.instantiate_exact::<xdg_wm_base::XdgWmBase>(1).unwrap(), */
    }
  }
  // Should change to be a Monitor struct, but I cannot find the wayland
  // calls to list monitors.
  pub fn monitors(&self) -> Vec<()> {
    let out: Main<wl_output::WlOutput> = self.globals.instantiate_exact(2).unwrap();
    dbg!(out);
    // Two monitors for now
    vec![(), ()]
  }
  pub fn add_win(&mut self, win: Window) {
    self.windows.push(win);
  }
  pub(crate) fn run(&mut self) {
    loop {
      self.data.borrow_mut().queue.dispatch(&mut (), |_, _, _| {}).unwrap();
    }
  }
}

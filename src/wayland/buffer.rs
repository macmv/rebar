use crate::wayland::backend::BackendData;
use common;

use rusttype::Point;
use std::{
  cell::RefCell,
  cmp::{max, min},
  fs,
  io::{Seek, SeekFrom, Write},
  os::unix::io::AsRawFd,
  rc::Rc,
};
use wayland_client::{
  protocol::{wl_buffer, wl_shm, wl_surface},
  Main,
};

#[derive(Clone)]
pub struct Buffer {
  wl_buf: Main<wl_buffer::WlBuffer>,
  surface: Main<wl_surface::WlSurface>,

  // Underlying memory-mapped file.
  buf: Rc<RefCell<fs::File>>,
  // Parent buffer. Buf will be copied to parent
  // on flush().
  parent: Option<Rc<Buffer>>,

  pub(crate) pos: Point<i32>,
  pub(crate) size: Point<u32>,

  background: common::Color,
}

impl common::Buffer for Buffer {
  fn draw_rect(&self, color: common::Color, x: i32, y: i32, width: u32, height: u32) {
    let mut buf = self.buf.borrow_mut();
    let col_bytes = &color.as_bytes();

    // Corners of the rectangle
    let x1 = max(0, x);
    let y1 = max(0, y);
    let x2 = min(self.size.x as i32, x + width as i32);
    let y2 = min(self.size.y as i32, y + height as i32);

    for y in y1..y2 {
      let index = ((y as u32 * self.size.x + x1 as u32) * 4).into();
      buf.seek(SeekFrom::Start(index)).unwrap();
      for _x in x1..x2 {
        // As is ok here, as the loop uses max(0, blah)
        buf.write(col_bytes).unwrap();
      }
    }
  }
  fn resize(&mut self, width: u32, height: u32) {
    let mut file = self.buf.borrow_mut();
    file.set_len(u64::from(width) * u64::from(height) * 4).unwrap();
    file.flush().unwrap();
  }
  fn draw_text(&self, _color: common::Color, _x: i32, _y: i32, _text: &str) -> u32 {
    0
  }
  fn set_pos(&mut self, x: i32, y: i32) {
    self.pos.x = x;
    self.pos.y = y;
  }
  fn set_background(&mut self, color: common::Color) {
    self.background = color;
  }
  fn get_background(&self) -> common::Color {
    self.background
  }
  fn x(&self) -> i32 {
    self.pos.x
  }
  fn y(&self) -> i32 {
    self.pos.y
  }
  #[inline]
  fn width(&self) -> u32 {
    self.size.x
  }
  #[inline]
  fn height(&self) -> u32 {
    self.size.y
  }
  fn flush(&self) {
    if self.parent.is_none() {
      return;
    }
    let p = self.parent.as_ref().unwrap();
    let mut buf = p.buf.borrow_mut();

    // Corners of the rectangle on the parent
    let x1 = max(0, self.pos.x);
    let y1 = max(0, self.pos.y);
    let x2 = min(p.size.x as i32, self.pos.x + self.size.x as i32);
    let y2 = min(p.size.y as i32, self.pos.y + self.size.y as i32);

    for y in y1..y2 {
      let index = ((y as u32 * p.size.x + x1 as u32) * 4).into();
      buf.seek(SeekFrom::Start(index)).unwrap();
      for x in x1..x2 {
        // X/Y will never be negative, as we call max(0, blah) above.
        // It will be negative if p.width or self.width is negative,
        // but we can assume that won't happen.
        let c = self.get(x as u32, y as u32);
        buf.write(&c.as_bytes()).unwrap();
      }
    }
  }
}

impl Buffer {
  /// This creates a new buffer. This will make a new buffer with memfd, and
  /// then make an shm buffer with that. Then it will create a surface, and
  /// call init() with that new surface. Finally, it commits the surface and
  /// syncs the queue in back.
  ///
  /// The init function is called at such a time that you can initialize a
  /// toplevel window in that function. This means this buffer can be used as
  /// a general purpose surface, or as an entire window.
  pub(crate) fn new(
    back: &mut BackendData,
    parent: Option<Rc<Buffer>>,
    fill: common::Color,
    pos: Point<i32>,
    size: Point<u32>,
  ) -> Self {
    // Create a buffer to write the window contents to
    let mut file = memfd::MemfdOptions::default().create("rebar-buffer").unwrap().into_file();

    let color_bytes = &fill.as_bytes();
    for _y in 0..size.y {
      for _x in 0..size.x {
        file.write(color_bytes).unwrap();
      }
    }
    file.flush().unwrap();

    // Make tmp a shared buffer
    let pool = back.shm.create_pool(
      file.as_raw_fd(),             // RawFd to the shared memory
      (size.x * size.y * 4) as i32, // size in bytes of the shared memory (4 bytes per pixel)
    );
    let wl_buf = pool.create_buffer(
      0,                        // Start of the buffer in the pool
      size.x as i32,            // width of the buffer in pixels
      size.y as i32,            // height of the buffer in pixels
      (size.x * 4) as i32,      // number of bytes between the beginning of two consecutive lines
      wl_shm::Format::Argb8888, // chosen encoding for the data
    );

    Buffer {
      wl_buf,
      surface: back.compositor.create_surface(),
      buf: Rc::new(RefCell::new(file)),
      parent,
      pos,
      size,
      background: common::Color::from_u32(0xff000000),
    }
  }
  pub(crate) fn init<F>(&self, back: Rc<RefCell<BackendData>>, init: F)
  where
    F: FnOnce(&Main<wl_surface::WlSurface>),
  {
    init(&self.surface);
    self.surface.commit();

    back
      .borrow_mut()
      .queue
      .sync_roundtrip(&mut (), |_, _, _| { /* we ignore unfiltered messages */ })
      .unwrap();

    self.surface.attach(Some(&self.wl_buf), 0, 0);
    self.surface.commit();
  }
  fn get(&self, x: u32, y: u32) -> common::Color {
    let index = ((y * self.size.x + x) * 4).into();
    let mut buf = self.buf.borrow_mut();
    buf.seek(SeekFrom::Start(index)).unwrap();
    return common::Color::read_bytes(buf.by_ref());
  }
  pub(crate) fn write_image(&self, _pos: Point<i32>, _size: Point<u32>, _data: &[u8]) {}
}

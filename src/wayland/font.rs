use rusttype::{Font, Point, Scale};

use crate::wayland::{backend::BackendData, buffer::Buffer, window::Window};
use common;
use common::Buffer as _;
use std::collections::HashMap;

#[derive(Copy, Clone)]
#[allow(dead_code)]
struct GlyphPosition {
  // X position withing the pixmap. Y will always be 0.
  x: u32,
  // The total size of the glyph, relative to it's top
  // left position.
  size: Point<u32>,
  // The origin of the glyph, relative to it's top left.
  // This point can be outside the glyph.
  origin: Point<i32>,
}

#[derive(Clone)]
pub(crate) struct FontCache<'a> {
  font: Font<'a>,
  buf: Buffer,
  chars: HashMap<char, GlyphPosition>,
  next_char: u32,
}

#[allow(dead_code)]
impl<'a> FontCache<'a> {
  pub(crate) fn new(back: &mut BackendData, font: Font<'a>) -> Self {
    let buf = Buffer::new(
      back,
      None,
      common::Color::from_u32(0x00000000),
      Point { x: 0, y: 0 },
      Point { x: 1, y: 1 },
    );

    FontCache { font, buf, chars: HashMap::new(), next_char: 0 }
  }
  fn draw(&mut self, mut x: i32, y: i32, text: &str, pixmap: xcb::Pixmap, win: &Window) {
    for c in text.chars() {
      x += self.draw_char(x, y, c, pixmap, win);
      // TODO: Custom character spacing
      x += 2;
    }
  }
  /// Draws a single character to the window. This will cache the character
  /// if it has not been drawn yet. Returns the width of the character that
  /// was drawn.
  fn draw_char(&mut self, x: i32, y: i32, c: char, pixmap: xcb::Pixmap, win: &Window) -> i32 {
    if !self.chars.contains_key(&c) {
      self.add(c, win);
    }
    let c = self.chars.get(&c).expect("add() should have added a rect to the chars map");
    self.copy_area(c, x, y, pixmap, win);
    (c.size.x + c.origin.x as u32) as i32
  }
  fn add(&mut self, c: char, _win: &Window) {
    // TODO: Font size
    let scaled = self.font.glyph(c).scaled(Scale::uniform(40.0));
    let positioned = scaled.positioned(Point { x: 0.0, y: 0.0 });
    let bounding_box = match positioned.pixel_bounding_box() {
      Some(b) => b,
      None => {
        let width = 20;
        let char_box = GlyphPosition {
          x: self.next_char,
          origin: Point { x: 0, y: 0 },
          size: Point { x: width, y: 0 },
        };
        self.next_char += width;
        self.chars.insert(c, char_box);
        return;
      }
    };

    // dbg!(c, bounding_box);
    let size = Point { x: bounding_box.width() as u32, y: bounding_box.width() as u32 };
    let char_box = GlyphPosition {
      x: self.next_char,
      origin: Point { x: bounding_box.min.x, y: bounding_box.min.y },
      // origin: Point { x: 0, y: bounding_box.min.y as i16 },
      size,
    };
    self.next_char += size.x;
    self.chars.insert(c, char_box);

    let mut data = vec![0; size.x as usize * size.y as usize * 4];

    // Draw the glyph to the buffer
    positioned.draw(|x, y, c| {
      let color = common::Color::from_grayscale((c * 256.0).floor() as u8);
      let bytes = color.as_bytes();
      let index = (y as usize * size.x as usize + x as usize) * 4;
      data[index + 0] = bytes[0];
      data[index + 1] = bytes[1];
      data[index + 2] = bytes[2];
      data[index + 3] = bytes[3];
    });

    // If the character box extends beyond the current
    // width/height of the pixmap, we make the pixmap
    // larger.
    let mut resize = false;
    let mut new_size = self.buf.size;
    while char_box.x + char_box.size.x > self.buf.size.x {
      new_size.x *= 2;
      resize = true;
    }
    while char_box.size.y > self.buf.size.y {
      new_size.y *= 2;
      resize = true;
    }
    if resize {
      self.buf.resize(new_size.x, new_size.y);
    }

    // By now, we have a pixmap large enough to support char_box.
    // So now we copy the data from the glyph onto the pixmap.
    self.buf.write_image(Point { x: char_box.x as i32, y: 0 }, size, data.as_slice());
  }
  fn copy_area(
    &self,
    _bounds: &GlyphPosition,
    _x: i32,
    _y: i32,
    _pixmap: xcb::Pixmap,
    _win: &Window,
  ) {
    // let conn = &win.back.conn;
    //
    // xcb::change_gc(conn, win.draw_ctx, &[(xcb::GC_FOREGROUND, 0xffffffff)]);
    // xcb::copy_area(
    //   conn,
    //   self.pixmap,
    //   pixmap,
    //   win.draw_ctx,
    //   bounds.x as i16,            // src x
    //   0,                          // src y
    //   bounds.origin.x + x as i16, // dst x
    //   bounds.origin.y + y as i16, // dst y
    //   bounds.size.x,              // width
    //   bounds.size.y,              // height
    // );
  }
}

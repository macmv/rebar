extern crate memfd;
extern crate wayland_client as wl_client;

pub mod backend;
pub mod buffer;
pub mod font;
pub mod window;

pub use crate::wayland::{backend::Backend, buffer::Buffer, window::Window};

use std::env;

pub fn active() -> bool {
  env::var("WAYLAND_DISPLAY").is_ok()
}

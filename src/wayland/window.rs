use crate::wayland::{
  backend::{Backend, BackendData},
  buffer::Buffer,
  font::FontCache,
};
use common;
use common::Buffer as _;

use rusttype::{Font, Point};
use std::{cell::RefCell, rc::Rc};
use wayland_protocols::wlr::unstable::layer_shell::v1::client::{
  zwlr_layer_shell_v1, zwlr_layer_surface_v1,
};

#[derive(Clone)]
pub struct Window {
  back: Rc<RefCell<BackendData>>,
  font: Rc<RefCell<FontCache<'static>>>,

  // Buffer covering the whole window. Not
  // to be confused with the individual buffers
  // used for each module.
  buffer: Rc<Buffer>,
}

impl common::Window for Window {
  fn create_buffer(&self, x: i32, y: i32, width: u32, height: u32) -> Box<dyn common::Buffer> {
    let buf = Buffer::new(
      &mut self.back.borrow_mut(),
      Some(self.buffer.clone()),
      common::Color::from_u32(0xff000000),
      Point { x, y },
      Point { x: width, y: height },
    );
    buf.init(self.back.clone(), |_| {});
    Box::new(buf)
  }
  fn width(&self) -> u32 {
    self.buffer.width()
  }
  fn height(&self) -> u32 {
    self.buffer.height()
  }
  fn name(&self) -> String {
    "DP-0".to_string()
  }
}

impl Window {
  pub(crate) fn new(back: &Backend, font: Font<'static>) -> Self {
    let size = Point { x: 1000, y: 50 };

    let mut data = back.data.borrow_mut();
    // This buffer has no parent, as it is a toplevel buffer.
    let buffer =
      Buffer::new(&mut data, None, common::Color::from_u32(0xff222222), Point { x: 0, y: 0 }, size);

    Window {
      back: back.data.clone(),
      font: Rc::new(RefCell::new(FontCache::new(&mut data, font))),
      buffer: Rc::new(buffer),
    }
  }
  pub(crate) fn init<F: Fn() + 'static>(&self, on_expose: F) {
    self.buffer.init(self.back.clone(), |surface| {
      // Make this window a bar
      let zwlr_surface = self.back.borrow().zwlr_shell.get_layer_surface(
        &surface,
        None,
        zwlr_layer_shell_v1::Layer::Top,
        "rebar".to_string(),
      );
      zwlr_surface.quick_assign(move |surface, event, _| match event {
        zwlr_layer_surface_v1::Event::Configure { serial, width: _, height: _ } => {
          on_expose();
          surface.ack_configure(serial);
        }
        _ => unreachable!(),
      });

      // Anchor the bar to the top, left and right.
      // This makes the bar act like a top-bar. For
      // a vertical bar, this should be top, bottom
      // and left.
      zwlr_surface.set_anchor(
        zwlr_layer_surface_v1::Anchor::Top
          | zwlr_layer_surface_v1::Anchor::Left
          | zwlr_layer_surface_v1::Anchor::Right,
      );
      // 8 px margin. Order is top, right, bottom, left.
      // The bottom margin appears to be ignored on sway.
      zwlr_surface.set_margin(8, 8, 100, 8);
      // If size is 0, then it will be anchored to the edge.
      // We want the bar to be 50 px tall, and span the whole
      // width.
      zwlr_surface.set_size(0, 50);
      // This acts weird. It appears to add some margin by default,
      // so this number is hard to get right.
      zwlr_surface.set_exclusive_zone(50 + 8);
    });
  }
}

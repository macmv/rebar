use crate::{
  bar,
  x11::{
    font::FontCache,
    window::{WMAtom, Window},
  },
};

use common::BarEvent;
use rusttype::Font;
use std::{collections::HashMap, rc::Rc, sync::mpsc, time};
use strum::IntoEnumIterator;
use xcb::randr;

// A backend for X11. Contains all windows and event handlers
// needed for any number of X11 bars.
pub struct Backend {
  bars: Vec<bar::Bar>,
  data: Rc<BackendData>,
  // A receiver for all events coming from all bars
  rx: mpsc::Receiver<BarEvent>,
  // Make of window ids to indexes into windows
  window_ids: HashMap<xcb::Window, usize>,
  pub(crate) windows: Vec<WindowCallbacks>,
}

// One of these is stored in Backend for each Window. Never
// passed anywhere.
pub(crate) struct WindowCallbacks {
  pub(crate) win: Window,
  pub(crate) on_expose: Option<Box<dyn Fn()>>,
}

// All global data for the backend. Passed to all windows.
pub(crate) struct BackendData {
  pub(crate) conn: xcb::Connection,
  // pub(crate) font: RefCell<FontCache<'static>>,
}

impl Backend {
  // This opens a connection to the X server. It will return an error
  // if a result could not be established.
  pub fn new(font: Font<'static>, rx: mpsc::Receiver<BarEvent>) -> Result<Self, xcb::ConnError> {
    let (conn, screen_num) = xcb::Connection::connect(None)?;

    // This is the xorg screen we are working with.
    // Even with multiple monitors, there is usually
    // only one screen.
    let scr = conn.get_setup().roots().nth(screen_num as usize).unwrap();
    let root = scr.root();

    // TODO: Possibly fix this? Lemonbar tries to do this in a different
    // way, which might be better.
    let visual = scr.root_visual();

    // TODO: Fix the hardcoded depth
    // It turns out passing xcb::COPY_FROM_PARENT
    // to create_pixmap() gives a BadValue error.
    // Hardcoding it to 24 seems to do the trick.
    let depth = 24;

    // List of names of atoms. Needed to find atom ids
    // using xcb.
    let mut atom_ids = vec![];

    for name in WMAtom::iter() {
      // This is the atom id that should be passed into xcb::change_property().
      let atom = xcb::intern_atom(&conn, false, &name.to_string()).get_reply().unwrap();
      atom_ids.push(atom.atom());
    }

    let data = Rc::new(BackendData { conn: conn });

    let monitors = randr::get_screen_resources_current(&data.conn, root).get_reply().unwrap();
    let mut window_ids = HashMap::new();
    let mut windows = vec![];
    for mon in monitors.outputs() {
      // Font internally is an arc, so clone() doesn't actually copy the entire font.
      let font = FontCache::new(font.clone());
      let res = Window::new(data.clone(), font, atom_ids.clone(), root, depth, visual, *mon);
      match res {
        Some(w) => {
          window_ids.insert(w.win, windows.len());
          windows.push(WindowCallbacks { win: w, on_expose: None });
        }
        None => {}
      };
    }

    data.conn.flush();

    if let Err(e) = data.conn.has_error() {
      println!("Conn has error: {}", e);
    }

    Ok(Backend { bars: vec![], data: data, window_ids: window_ids, windows: windows, rx: rx })
  }
  pub(crate) fn set_bars(&mut self, bars: Vec<bar::Bar>) {
    self.bars = bars;
  }
  pub(crate) fn run(&mut self) {
    loop {
      if let Some(e) = self.data.conn.poll_for_event() {
        match e.response_type() {
          xcb::EXPOSE => {
            // I hate this just as much as you do, but this is what it shows in the
            // examples.
            let expose: &xcb::ExposeEvent = unsafe { xcb::cast_event(&e) };

            let index = *self.window_ids.get(&expose.window()).unwrap();
            let win = &self.windows[index];
            if let Some(f) = &win.on_expose {
              f();
            }

            self.data.conn.flush();
            if let Err(e) = self.data.conn.has_error() {
              println!("Conn has error: {}", e);
            }
          }
          xcb::NO_EXPOSURE => {
            // println!("No exposure event");
          }
          other => println!("Got unknown event from X server: {}", other),
        }
      }
      if let Ok(e) = self.rx.recv_timeout(time::Duration::from_millis(10)) {
        self.bars[e.bar].handle_event(&e);

        self.data.conn.flush();
        if let Err(e) = self.data.conn.has_error() {
          println!("Conn has error: {}", e);
        }
      }
    }
  }
}

use crate::x11::window::Window;
use common;

use std::convert::TryInto;

#[derive(Clone)]
pub struct Buffer {
  pub(crate) win: Window,
  pub(crate) pixmap: xcb::Pixmap,
  pub(crate) x: i16,
  pub(crate) y: i16,
  pub(crate) width: u16,
  pub(crate) height: u16,
  pub(crate) background: common::Color,
}

impl common::Buffer for Buffer {
  fn draw_rect(&self, color: common::Color, x: i32, y: i32, width: u32, height: u32) {
    xcb::change_gc(&self.win.back.conn, self.win.draw_ctx, &[(xcb::GC_FOREGROUND, color.as_u32())]);
    xcb::poly_fill_rectangle(
      &self.win.back.conn,
      self.pixmap,
      self.win.draw_ctx,
      &[xcb::Rectangle::new(
        x.try_into().unwrap(),
        y.try_into().unwrap(),
        width.try_into().unwrap(),
        height.try_into().unwrap(),
      )],
    );
  }
  fn resize(&mut self, width: u32, height: u32) {
    let conn = &self.win.back.conn;
    let width = width.try_into().unwrap();
    let height = height.try_into().unwrap();

    // Need to pass a valid win, but it doesn't matter that we use the same pixmap
    // on multiple windows.
    let new_pixmap = conn.generate_id();
    xcb::create_pixmap(conn, 24, new_pixmap, self.win.win, width, height);

    // Copy from old pixmap (self.pixmap) to new pixmap
    xcb::copy_area(
      conn,
      self.pixmap,
      new_pixmap,
      self.win.draw_ctx,
      0,           // src x
      0,           // src y
      0,           // dest x
      0,           // dest y
      self.width,  // width
      self.height, // height
    );
    if width > self.width || height > self.height {
      xcb::change_gc(
        &self.win.back.conn,
        self.win.draw_ctx,
        &[(xcb::GC_FOREGROUND, self.background.as_u32())],
      );
      // Clear the new portion of the pixmap
      let mut args = vec![];
      if width > self.width {
        // The right column
        args.push(xcb::Rectangle::new(self.width as i16, 0, width - self.width, height));
      }
      if height > self.height {
        // The left bottom quadrant
        args.push(xcb::Rectangle::new(
          0,
          self.height as i16,
          self.width - width,
          self.height - height,
        ));
      }
      xcb::poly_fill_rectangle(conn, new_pixmap, self.win.draw_ctx, &args);
    }

    // Replace the old pixmap with the new one
    xcb::free_pixmap(conn, self.pixmap);
    self.pixmap = new_pixmap;
    self.width = width;
    self.height = height;
  }
  fn draw_text(&self, color: common::Color, x: i32, y: i32, text: &str) -> u32 {
    self.win.font.borrow_mut().draw(x, y, text, color, self.background, self.pixmap, &self.win)
  }
  fn set_pos(&mut self, x: i32, y: i32) {
    self.x = x.try_into().unwrap();
    self.y = y.try_into().unwrap();
  }
  fn set_background(&mut self, color: common::Color) {
    self.background = color;
  }
  fn get_background(&self) -> common::Color {
    self.background
  }
  fn x(&self) -> i32 {
    self.x as i32
  }
  fn y(&self) -> i32 {
    self.y as i32
  }
  fn width(&self) -> u32 {
    self.width as u32
  }
  fn height(&self) -> u32 {
    self.height as u32
  }
  fn flush(&self) {
    xcb::copy_area(
      &self.win.back.conn,
      self.pixmap,
      self.win.win,
      self.win.draw_ctx,
      0,           // src x
      0,           // src y
      self.x,      // dest x
      self.y,      // dest y
      self.width,  // width
      self.height, // height
    );
  }
}

impl Drop for Buffer {
  fn drop(&mut self) {
    xcb::free_pixmap(&self.win.back.conn, self.pixmap);
  }
}

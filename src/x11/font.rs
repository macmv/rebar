extern crate test;

use crate::x11::window::Window;
use common;

use rusttype::{Font, Point, Scale};
use std::collections::HashMap;

pub(crate) struct FontCache<'a> {
  font: Font<'a>,
  // Map of all baked characters.
  chars: HashMap<char, Glyph>,
}

#[derive(Clone)]
struct Glyph {
  // The total size of the glyph, relative to it's top
  // left position.
  size: Point<u16>,
  // The origin of the glyph, relative to it's top left.
  // This point can be outside the glyph.
  origin: Point<i16>,
  // The data. Each byte is a pixel's "brightness". This
  // should be passed to lerp() with whatever foreground
  // and background color this font is being rendered in.
  // This is indexed with y * size.x + x. size.x is width.
  data: Vec<u8>,
}

impl<'a> FontCache<'a> {
  pub(crate) fn new(font: Font<'a>) -> Self {
    FontCache { font, chars: HashMap::new() }
  }
  pub(crate) fn draw(
    &mut self,
    mut x: i32,
    y: i32,
    text: &str,
    front: common::Color,
    back: common::Color,
    pixmap: xcb::Pixmap,
    win: &Window,
  ) -> u32 {
    let start = x as u32;
    for c in text.chars() {
      x += self.draw_char(x, y, c, front, back, pixmap, win);
      // TODO: Custom character spacing
      x += 2;
    }
    x as u32 - start
  }
  /// Draws a single character to the window. This will cache the character
  /// if it has not been drawn yet. Returns the width of the character that
  /// was drawn.
  fn draw_char(
    &mut self,
    x: i32,
    y: i32,
    c: char,
    front: common::Color,
    back: common::Color,
    pixmap: xcb::Pixmap,
    win: &Window,
  ) -> i32 {
    if !self.chars.contains_key(&c) {
      self.add(c, win);
    }
    let c = self.chars.get(&c).expect("add() should have added a glyph to the chars map");
    self.copy_area(c, x as i16, y as i16, front, back, pixmap, win);
    (c.size.x + c.origin.x as u16) as i32
  }
  fn add(&mut self, c: char, win: &Window) {
    let conn = &win.back.conn;
    // Always work in white for these pixmaps. We set the colors in the buffer,
    // so that we can lerp front and background correctly.
    xcb::change_gc(conn, win.draw_ctx, &[(xcb::GC_FOREGROUND, 0xffffffff)]);

    // TODO: Font size
    let scaled = self.font.glyph(c).scaled(Scale::uniform(40.0));
    let positioned = scaled.positioned(Point { x: 0.0, y: 0.0 });
    let bounding_box = match positioned.pixel_bounding_box() {
      Some(b) => b,
      None => {
        // TODO: Find a way to get the width of a space.
        let width = 10;
        // Height is zero, so we have empty data.
        let char_box =
          Glyph { origin: Point { x: 0, y: 0 }, size: Point { x: width, y: 0 }, data: vec![] };
        self.chars.insert(c, char_box);
        return;
      }
    };

    // dbg!(c, bounding_box);
    let width = bounding_box.width() as u32;
    let height = bounding_box.height() as u32;
    let mut data = vec![0; width as usize * height as usize];

    // Draw the glyph to the buffer
    positioned.draw(|x, y, c| {
      let index = y as usize * width as usize + x as usize;
      data[index] = (c * 255.0) as u8;
    });

    let char_box = Glyph {
      origin: Point { x: bounding_box.min.x as i16, y: bounding_box.min.y as i16 },
      size: Point { x: width as u16, y: height as u16 },
      data,
    };
    self.chars.insert(c, char_box);
  }
  fn copy_area(
    &self,
    g: &Glyph,
    x: i16,
    y: i16,
    front: common::Color,
    back: common::Color,
    pixmap: xcb::Pixmap,
    win: &Window,
  ) {
    let conn = &win.back.conn;

    let mut data = vec![0; g.size.x as usize * g.size.y as usize * 4];
    for (i, v) in g.data.iter().enumerate() {
      let bytes = common::Color::lerp(back, front, *v as f64 / 255.0).as_bytes();
      let index = i as usize * 4;
      data[index + 0] = bytes[0];
      data[index + 1] = bytes[1];
      data[index + 2] = bytes[2];
      data[index + 3] = bytes[3];
    }

    xcb::change_gc(conn, win.draw_ctx, &[(xcb::GC_FOREGROUND, 0xffffffff)]);
    xcb::put_image(
      conn,
      xcb::IMAGE_FORMAT_Z_PIXMAP as u8, // format
      pixmap,
      win.draw_ctx,
      g.size.x,       // width
      g.size.y,       // height
      g.origin.x + x, // dst x
      g.origin.y + y, // dst y
      0,              // left pad
      24,             // depth
      data.as_slice(),
    );
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::x11::{backend, font::FontCache, window};
  use rusttype::Font;
  use std::{rc::Rc, time::Instant};
  use strum::IntoEnumIterator;
  use test::Bencher;
  use xcb::randr;

  #[bench]
  fn bench_xcb_put_image(b: &mut Bencher) {
    let font_data = include_bytes!(
      "/usr/share/fonts/nerd-fonts-complete/TTF/Noto Sans Regular Nerd Font Complete.ttf"
    );
    let font = Font::try_from_bytes(font_data).expect("Could not load font!");

    let (conn, screen_num) = xcb::Connection::connect(None).unwrap();
    let scr = conn.get_setup().roots().nth(screen_num as usize).unwrap();
    let root = scr.root();
    let visual = scr.root_visual();
    let depth = 24;

    let mut atom_ids = vec![];
    for name in window::WMAtom::iter() {
      // This is the atom id that should be passed into xcb::change_property().
      let atom = xcb::intern_atom(&conn, false, &name.to_string()).get_reply().unwrap();
      atom_ids.push(atom.atom());
    }
    let data = Rc::new(backend::BackendData { conn: conn });

    let monitors = randr::get_screen_resources_current(&data.conn, root).get_reply().unwrap();
    let mut window = None;
    for mon in monitors.outputs() {
      // This is not going to be used.
      let font = FontCache::new(font.clone());
      let res = Window::new(data.clone(), font, atom_ids.clone(), root, depth, visual, *mon);
      match res {
        Some(w) => {
          window = Some(w);
          break;
        }
        None => {}
      };
    }
    // We just want one window.
    let win = window.unwrap();

    b.iter(move || {
      let mut fc = FontCache::new(font.clone());
      for c in 'a'..'z' {
        let start = Instant::now();
        fc.add(c, &win);
        dbg!(start.elapsed());
      }
    });
  }
}

use crate::x11::{backend::BackendData, buffer::Buffer, font::FontCache};
use common;

use convert_case::{Case, Casing};
use std::{cell::RefCell, convert::TryInto, fmt, rc::Rc};
use strum_macros::EnumIter;
use xcb::randr;

// See https://specifications.freedesktop.org/wm-spec/1.3/ar01s05.html for more.
// Every one of these values will be converted to snake case, capitalized, and
// prepended with an underscore. See impl fmt::Display for WMAtom.
#[derive(Debug, EnumIter)]
pub enum WMAtom {
  NetWMDesktop,
  NetWMWindowType,
  NetWMWindowTypeDesktop,
  NetWMWindowTypeDock,
  NetWMWindowTypeToolbar,
  NetWMWindowTypeMenu,
  NetWMWindowTypeUtility,
  NetWMWindowTypeSplash,
  NetWMWindowTypeDialog,
  NetWMWindowTypeNormal,
  NetWMState,
  NetWMStateSticky,
  NetWMStateAbove,
  NetWMStrut,
  NetWMStrutPartial,
}

impl fmt::Display for WMAtom {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    // This converts NetWMWindowType -> _NET_WM_WINDOW_TYPE
    // The underscore before it is needed, and to_case() will
    // remove an underscore before it, so we need to use
    // format! and write!.
    write!(f, "_{}", format!("{:?}", self).to_case(Case::Snake).to_uppercase())
  }
}

#[derive(Clone)]
pub struct Window {
  pub(crate) back: Rc<BackendData>,
  pub(crate) font: Rc<RefCell<FontCache<'static>>>,

  // SAFETY: The xcb crate does not automatically free these!
  // I have implemented Drop for Window, but any additional
  // xcb fields must also be freed.
  pub(crate) win: xcb::Window,
  pub(crate) pixmap: xcb::Pixmap,
  pub(crate) draw_ctx: xcb::Gc,
  pub(crate) clear_ctx: xcb::Gc,

  buffers: Vec<Buffer>,

  name: String,
  pub(crate) x: i16,
  pub(crate) y: i16,
  pub(crate) width: u16,
  pub(crate) height: u16,
}

impl Window {
  pub(crate) fn new(
    back: Rc<BackendData>,
    font: FontCache<'static>,
    atom_ids: Vec<u32>,
    root: xcb::Window,
    depth: u8,
    visual: u32,
    mon: u32,
  ) -> Option<Self> {
    let output = randr::get_output_info(&back.conn, mon, xcb::CURRENT_TIME).get_reply().unwrap();
    if output.connection() != randr::CONNECTION_CONNECTED as u8 {
      return None;
    }
    let crtc =
      randr::get_crtc_info(&back.conn, output.crtc(), xcb::CURRENT_TIME).get_reply().unwrap();

    let mut win = Window {
      back: back,
      font: Rc::new(RefCell::new(font)),
      draw_ctx: 0,
      clear_ctx: 0,
      buffers: vec![],
      name: String::from_utf8(output.name().to_vec()).unwrap(),
      win: 0,
      pixmap: 0,
      x: crtc.x(),
      y: crtc.y(),
      width: crtc.width(),
      height: 50,
    };
    win.create(atom_ids, root, depth, visual);
    Some(win)
  }
  fn create(&mut self, atom_ids: Vec<u32>, root: xcb::Window, depth: u8, visual: u32) {
    // Unclear if I need this. I will leave it commented out for now (aka forever).
    // NOTE: If I re-add the colormap for any reason, it must be stored within the
    // Window so that it can be freed properly!
    // let colormap = self.conn.generate_id();
    // xcb::create_colormap(&self.conn, xcb::COLORMAP_ALLOC_NONE as u8, colormap,
    // scr.root(), visual);

    self.win = self.back.conn.generate_id();
    xcb::create_window(
      &self.back.conn,
      depth,
      self.win,
      root,
      self.x,
      self.y,
      self.width,
      self.height,
      0, // Border width, should always be 0
      xcb::WINDOW_CLASS_INPUT_OUTPUT as u16,
      visual,
      &[
        // If set, then the window controls it's own position, instead of the window manager.
        // However, this also means the wm will not allocate space at the top of the screen
        // for the bar. So, this should probably be a setting that can be changed in the config.
        (xcb::CW_OVERRIDE_REDIRECT, 0), // Is a bool
        // Clear pixel (gray color).
        (xcb::CW_BACK_PIXEL, 0xff000000),
        // Border color (gray color).
        (xcb::CW_BORDER_PIXEL, 0xff000000),
        // We want expose and mouse click events.
        (xcb::CW_EVENT_MASK, xcb::EVENT_MASK_EXPOSURE | xcb::EVENT_MASK_BUTTON_PRESS),
        // (xcb::CW_COLORMAP, colormap),
      ],
    );

    // Must send these atoms before mapping the window
    self.send_atoms(atom_ids);

    self.pixmap = self.back.conn.generate_id();
    xcb::create_pixmap(&self.back.conn, depth, self.pixmap, self.win, self.width, self.height);

    self.draw_ctx = self.back.conn.generate_id();
    self.clear_ctx = self.back.conn.generate_id();

    xcb::create_gc(
      &self.back.conn,
      self.draw_ctx,
      self.pixmap,
      &[(xcb::GC_FOREGROUND, 0xffffffff)],
    );
    xcb::create_gc(
      &self.back.conn,
      self.clear_ctx,
      self.pixmap,
      &[(xcb::GC_FOREGROUND, 0xff000000)],
    );

    // Clear pixmap
    xcb::poly_fill_rectangle(
      &self.back.conn,
      self.pixmap,
      self.clear_ctx,
      &[xcb::Rectangle::new(0, 0, self.width, self.height)],
    );

    // Map window
    xcb::map_window(&self.back.conn, self.win);
  }
  fn send_atoms(&self, atom_ids: Vec<u32>) {
    // let values = &[
    //   (xcb::CONFIG_WINDOW_SIBLING as u16, 0x1e2),
    //   (xcb::CONFIG_WINDOW_STACK_MODE as u16, xcb::STACK_MODE_ABOVE),
    // ];
    // xcb::configure_window(&self.conn, self.window, values);

    // These are the amouts of space to allocate at the sides of the screen
    // The first four numbers are for adding some padding on those sides of
    // the screen. The last eight numbers are for how wide/tall those paddings
    // should be. Only the first four numbers are sent for legacy wms, so I
    // don't know if you can rely on the last eight numbers to make two bars
    // next to each other (after a little bit of testing, all of these values
    // appear to be ignored by i3, which will just allocate space if you set
    // the window type to docked).
    let strut = vec![
      0,              // left
      0,              // right
      self.height,    // top
      0,              // bottom
      0,              // left_start_y
      0,              // left_end_y
      0,              // right_start_y
      0,              // right_end_y
      0,              // top_start_x (left side of bar)
      0 + self.width, // top_end_x (right side of bar)
      0,              // bottom_start_x
      0,              // bottom_end_x
    ];

    // Set window type to docked
    xcb::change_property(
      &self.back.conn,
      xcb::PROP_MODE_REPLACE as u8,
      self.win,
      atom_ids[WMAtom::NetWMWindowType as usize],
      xcb::ATOM_ATOM,
      32,
      &[atom_ids[WMAtom::NetWMWindowTypeDock as usize]],
    );
    // Make the window sticky
    xcb::change_property(
      &self.back.conn,
      xcb::PROP_MODE_APPEND as u8,
      self.win,
      atom_ids[WMAtom::NetWMState as usize],
      xcb::ATOM_ATOM,
      32,
      &[atom_ids[WMAtom::NetWMStateSticky as usize]],
    );
    // Set the desktop
    xcb::change_property(
      &self.back.conn,
      xcb::PROP_MODE_REPLACE as u8,
      self.win,
      atom_ids[WMAtom::NetWMDesktop as usize],
      xcb::ATOM_CARDINAL,
      32,
      // Want to be in first desktop
      &[0],
    );
    // Set the window struts
    xcb::change_property(
      &self.back.conn,
      xcb::PROP_MODE_REPLACE as u8,
      self.win,
      atom_ids[WMAtom::NetWMStrutPartial as usize],
      xcb::ATOM_CARDINAL,
      32,
      &strut[..12],
    );
    // Set the window struts (legacy, still should do it. Doesn't appear
    // to be needed on i3).
    xcb::change_property(
      &self.back.conn,
      xcb::PROP_MODE_REPLACE as u8,
      self.win,
      atom_ids[WMAtom::NetWMStrut as usize],
      xcb::ATOM_CARDINAL,
      32,
      &strut[..4],
    );
    // Set the name of the window
    xcb::change_property(
      &self.back.conn,
      xcb::PROP_MODE_REPLACE as u8,
      self.win,
      xcb::ATOM_WM_NAME,
      xcb::ATOM_STRING,
      8,
      "Rebar".as_bytes(),
    );
  }
}

impl common::Window for Window {
  /// Creates an empty buffer. Since this is on X11, the width/height will be
  /// cast to u16.
  fn create_buffer(&self, x: i32, y: i32, width: u32, height: u32) -> Box<dyn common::Buffer> {
    let w: u16 = width.try_into().unwrap();
    let h: u16 = height.try_into().unwrap();

    let pixmap = self.back.conn.generate_id();
    // TODO: Fix hardcoded depth
    xcb::create_pixmap(&self.back.conn, 24, pixmap, self.win, w, h);
    // TODO: Validate width/height to be >= 0,
    // and make sure x/y are i16s. If any of
    // these are invalid, then we should return
    // and do nothing.
    xcb::poly_fill_rectangle(
      &self.back.conn,
      pixmap,
      self.clear_ctx,
      &[xcb::Rectangle::new(0, 0, w, h)],
    );
    Box::new(Buffer {
      win: self.clone(),
      pixmap: pixmap,
      x: x.try_into().unwrap(),
      y: y.try_into().unwrap(),
      width: w,
      height: h,
      background: common::Color::from_u32(0xff000000),
    })
  }
  fn width(&self) -> u32 {
    self.width.into()
  }
  fn height(&self) -> u32 {
    self.height.into()
  }
  fn name(&self) -> String {
    self.name.clone()
  }
}

impl Drop for Window {
  fn drop(&mut self) {
    let conn = &self.back.conn;
    xcb::free_gc(conn, self.draw_ctx);
    xcb::free_gc(conn, self.clear_ctx);
    xcb::free_pixmap(conn, self.pixmap);
    xcb::destroy_window(conn, self.win);
  }
}
